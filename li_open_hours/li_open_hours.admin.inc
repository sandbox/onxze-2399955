<?php

/**
 * @file
 * Library index Open Hours settings form.
 */

/**
 * Set fields for module settings, shown to adminitrator.
 *
 * @return array field description for settings
 */
function li_open_hours_admin($form, &$form_state) {
  $form = array();

  drupal_add_css(drupal_get_path('module', 'li_open_hours') . '/li_open_hours.css', 'module', 'all', FALSE);

  $form['ldb_block'] = array(
    '#type' => 'markup',
    '#markup' => '<div><h2>' . t('Library Index Open Hours settings for block.') . '</h2><p>' . t(
      'Select content type on which full view block shown and field that has the value of current library.') . '</p></div>'
  );
  // System content types
  $content_types = node_type_get_types();
  $ct_option = array('none' => t('Select content type'));
  foreach ($content_types as $value) {
    $ct_option[$value->type] = $value->name;
  }
  $selected_option = variable_get('lioh_content_type', 'none');
  $ctf_option = array('none' => t('Select field'));
  $form['lioh_content_type'] = array(
      '#title' => t('System Content types'),
      '#type' => 'select',
      '#description' => (t('Content type to which block is tied.')),
      '#options' => $ct_option,
      '#default_value' => $selected_option,
      '#ajax' => array(
        'wrapper' => 'content_type-field-fieldset-wrapper',
        'callback' => 'li_open_hours_content_type_field_select_callback',
      ),
  );

  $form['field_fieldset'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="content_type-field-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  if (!empty($form_state['values']['lioh_content_type'])) {
    $selected_option = $form_state['values']['lioh_content_type'];
  }
  if ($selected_option != 'none') {
    // Fields for selected content type
    $fields = field_info_instances('node', $selected_option);
    foreach ($fields as $value) {
      $ctf_option[$value['field_name']] = $value['label'];
    }
    $selected_field = variable_get('lioh_lid_field', 'none');
    $form['field_fieldset']['lioh_lid_field'] = array(
        '#title' => t('Library Id field'),
        '#type' => 'select',
        '#description' => (t('Field that gives the library Id.')),
        '#options' => $ctf_option,
        '#default_value' => $selected_field,
    );
  }

  return system_settings_form($form);
}

/**
 * Callback for the select element.
 * This just selects content type's fields and returns the field_fieldset.
 */
function li_open_hours_content_type_field_select_callback($form, $form_state) {
  return $form['field_fieldset'];
}
