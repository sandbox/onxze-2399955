<?php
/*
 * @file
 * Field for library's information.
 */


/**
 * Implements hook_field_info().
 */
function li_library_info_field_info() {
  return array(
      'li_library_info' => array(
          'label' => t('Library information'),
          'description' => t('Library information from kirjastot.fi library index'),
          'default_widget' => 'li_library_info_id',
          'default_formatter' => 'li_library_info_texts',
      ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function li_library_info_field_is_empty($item, $field) {
  return empty($item['value']);
}

function li_library_info_field_formatter_info() {
  return array(
      'li_library_info_texts' => array(
          'label' => t('Simple text-based formatter'),
          'field types' => array('li_library_info'),
          'settings' => array(
            'show_name' => FALSE,
            'show_description' => FALSE,
            'show_oh' => FALSE,
            'show_telephones' => FALSE,
            'show_directions' => FALSE,
            'show_parking' => FALSE,
            'show_websites' => FALSE,
            'show_street_address' => FALSE,
            'show_mail_address' => FALSE,
            'show_coordinates' => FALSE,
            'show_homepage' => FALSE,
            'image_size' => 0,
          ),
      ),
  );
}

/**
 * Implements hook_field_formater_view().
 */
function li_library_info_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'li_library_info_texts':
      drupal_add_css(drupal_get_path('module', 'library_index') . '/library_index.css', 'module', 'all', FALSE);
      foreach ($items as $delta => $item) {
        $data = library_index_library_info($item['value']);

        $options = array('lib_info' => $data, 'settings' => $settings);
        if ($settings['show_oh']) {
          drupal_add_library('system', 'drupal.ajax');
          $data = library_index_get_open_hours($item['value']);
          $options['library_open_hours'] = theme('li_open_hours_field', array('open_hours' => $data, 'lid' => $item['value']));
        }
        $element[$delta] = array(
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => theme('li_library_info_field', $options),
        );
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function li_library_info_field_widget_info() {
  return array(
      'li_library_info_id' => array(
          'label' => t('Library Id'),
          'field types' => array('li_library_info'),
      ),
      'li_library_info_list' => array(
          'label' => t('Library Id from library list'),
          'field types' => array('li_library_info'),
      ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function li_library_info_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['value']) ? $items[$delta]['value'] : '';

  $widget = $element;
  $widget['#delta'] = $delta;

  switch ($instance['widget']['type']) {

    case 'li_library_info_id':
      $widget += array(
          '#type' => 'textfield',
          '#default_value' => $value,
          '#size' => 7,
          '#maxlength' => 7,
      );
      break;

    case 'li_library_info_list':
      $options = library_index_library_list(t('No selection'));
      $widget += array(
          '#type' => 'select',
          '#title' => t('Selected library'),
          '#options' => $options,
          '#default_value' => $value,
      );
      break;
  }

  $element['value'] = $widget;
  return $element;
}

/**
 * Implementation of hook_theme().
 */
function li_library_info_theme() {
  $themes = array(
      'li_library_info_field' => array(
          'arguments' => array('result' => NULL),
          'template' => 'li-library-info-field',
      ),
  );
  return $themes;
}

/**
 * Process variables for li_library_info_field.tpl.php.
 *
 * @see li-library-info-field.tpl.php
 */
function template_preprocess_li_library_info_field(&$variables) {
  $data = $variables['lib_info'];

  $settings = $variables['settings'];
  $image_size = $settings['image_size'];

  global $language;
  $lang = $language->language;

  if ($settings['show_name']) {
    if (!empty($data->name->$lang)) {
      $variables['library_name'] = $data->name->$lang;
    }
    else {
      $variables['library_name'] = $data->name->fi;
    }
  }

  if (isset($library_open_hours)) {
    $variables['library_open_hours'] = $library_open_hours;
  }

  if ($settings['show_telephones']) {
    $variables['library_telephones'] = t('Telephone numbers');
    $telephones = $data->contact->telephones;
    $i = 0;
    foreach ($telephones as $tel) {
      $variables['library_tels'][$i]['title'] = empty($tel->name->$lang) ? '&nbsp;' : $tel->name->$lang;
      $variables['library_tels'][$i]['number'] = $tel->number;
      $variables['library_tels'][$i]['description'] = $tel->description->$lang;
      $i++;
    }
  }

  if ($settings['show_street_address']) {
    $variables['library_visit_address'] = t('Visit address');
    $visit = $data->street_address;
    $variables['library_vaddress_street'] = $visit->street->$lang;
    $variables['library_vaddress_area'] = $visit->area->$lang;
    $variables['library_vaddress_zipcode'] = $visit->zipcode;
    $variables['library_vaddress_city'] = $visit->city->$lang;
  }

  if ($settings['show_mail_address']) {
    $variables['library_postal_postal'] = t('Postal address');
    $postal = $data->mail_address;
    $variables['library_paddress_post_box'] = $postal->post_box;
    $variables['library_paddress_post_address'] = $postal->post_address->$lang;
    $variables['library_paddress_post_office'] = $postal->post_office->$lang;
    $variables['library_paddress_zipcode'] = $postal->zipcode;
  }

  if ($settings['show_coordinates']) {
    // Coordinates
    $variables['library_coord_lat'] = $data->coordinates->lat;
    $variables['library_coord_lon'] = $data->coordinates->lon;
  }

  if (isset($data->pictures[0]) && $image_size != 'no_image') {
    if ($data->pictures[0]->files->$image_size !== NULL) {
      $variables['library_image_file'] = $data->pictures[0]->files->$image_size;
      if (empty($data->pictures[0]->name->$lang)) {
        $variables['library_image_alt'] = $data->pictures[0]->name->fi;
      }
      else {
        $variables['library_image_alt'] = $data->pictures[0]->name->$lang;
      }
    }
  }

}

function li_library_info_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $form = array();
  if ($display['type'] == 'li_library_info_texts') {
    $form['show_name'] = array(
      '#title' => t('Show  name'),
      '#type' => 'checkbox',
      '#default_value' => $settings['show_name'],
    );
    $form['show_description'] = array(
      '#title' => t('Show description'),
      '#type' => 'checkbox',
      '#default_value' => $settings['show_description'],
    );
    $form['show_oh'] = array(
      '#title' => t('Show Open Hours'),
      '#type' => 'checkbox',
      '#default_value' => $settings['show_oh'],
    );
    $form['show_telephones'] = array(
      '#title' => t('Show telephones'),
      '#type' => 'checkbox',
      '#default_value' => $settings['show_telephones'],
    );
    // $form['show_directions'] = array(
    //   '#title' => t('Show directions'),
    //   '#type' => 'checkbox',
    //   '#default_value' => $settings['show_directions'],
    // );
    // $form['show_parking'] = array(
    //   '#title' => t('Show parking'),
    //   '#type' => 'checkbox',
    //   '#default_value' => $settings['show_parking'],
    // );
    // $form['show_websites'] = array(
    //   '#title' => t('Show websites'),
    //   '#type' => 'checkbox',
    //   '#default_value' => $settings['show_websites'],
    // );
    $form['show_street_address'] = array(
      '#title' => t('Show street address'),
      '#type' => 'checkbox',
      '#default_value' => $settings['show_street_address'],
    );
    $form['show_mail_address'] = array(
      '#title' => t('Show mail address'),
      '#type' => 'checkbox',
      '#default_value' => $settings['show_mail_address'],
    );
    $form['show_coordinates'] = array(
      '#title' => t('Show  coordinates'),
      '#type' => 'checkbox',
      '#default_value' => $settings['show_coordinates'],
    );
    // $form['show_homepage'] = array(
    //   '#title' => t('Show  homepage'),
    //   '#type' => 'checkbox',
    //   '#default_value' => $settings['show_homepage'],
    // );
    $options = array(
      'no_image' => t('No image'),
      'small' => t('Small'),
      'medium' => t('Medium'),
      'large' => t('Large'),
      'huge' => t('Huge'),
    );
    $form['image_size'] = array(
      '#title' => t('Image size'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $settings['image_size'],
    );
// branch type
// consortium
// provincial area
// isil
// name short
// description
// type
  }
  return $form;
}

function li_library_info_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = '';
  if ($display['type'] == 'li_library_info_texts') {
    $summary = t('Show') . ' ';
    $summary .= $settings['show_name'] ? t('name') . ', ' : '';
    $summary .= $settings['show_description'] ? t('description') . ', ' : '';
    $summary .= $settings['show_oh'] ? t('open hours') . ', ' : '';
    $summary .= $settings['show_telephones'] ? t('telephones') . ', ' : '';
    $summary .= $settings['show_directions'] ? t('directions') . ', ' : '';
    $summary .= $settings['show_parking'] ? t('parking') . ', ' : '';
    $summary .= $settings['show_websites'] ? t('websites') . ', ' : '';
    $summary .= $settings['show_street_address'] ? t('street address') . ', ' : '';
    $summary .= $settings['show_mail_address'] ? t('mail address') . ', ' : '';
    $summary .= $settings['show_coordinates'] ? t('coordinates') . ', ' : '';
    $summary .= $settings['show_homepage'] ? t('homepage') . ', ' : '';


    $options = array(
      'no_image' => t('No image'),
      'small' => t('Small'),
      'medium' => t('Medium'),
      'large' => t('Large'),
      'huge' => t('Huge'),
    );
    $summary .= '<br />' . t('library image size') . ': ';
    $summary .= strtolower($options[$settings['image_size']]) . '.';
  }
  return $summary;
}
