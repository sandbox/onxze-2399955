-- SUMMARY --

Library Index module is the common Api towards api.kirjastot.fi to get information about libraries.
Library Open Hours retrieves open hour information from api.kirjastot.fi.
Library Information retrieves basic information from api.kirjastot.fi
 - Library name
 - Description
 - Open hours, if open hours module enabled and selected to show
 - Telephone numbers
 - Visit address
 - Postal address
 - Coordinates
 - Library picture


For a full description of the module, visit the sandbox project page:
  https://www.drupal.org/sandbox/onxze/2399955

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/2399955


-- REQUIREMENTS --

Needs curl.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

Library Index Api module has settings:

Library Index Api address
  Address of "Kirjatohakemisto" api, defaults to "http://api.kirjastot.fi/v2/".

Cache timeout
  Timeout for Library information to be stored cache, defaults 0 (no cache)

Library Index Consortium
  If consortium name is given, fetch list of all libraries in consortium and their library ID.
  If set, also activates field widget to list libaries by names where to choose.

Library Open Hours module has settings:

System Content types and Library Id field
  If Library Open Hours shown at block, these fields determine at which content type page the block is shown and which field tells the library ID. Block is shown only at full text view. 



-- CUSTOMIZATION --

Own layouts for fields and blocks can be done by overriding field and block templates.


-- TROUBLESHOOTING --



-- FAQ --



-- CONTACT --

Current maintainers:
* Mika Hatakka (onxze) - http://drupal.org/user/735636
